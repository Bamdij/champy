<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdentifiantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('identifiants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Nom Equipe');
            $table->string('Prenom coach');
            $table->string('Nom coach');
            $table->string('Nombre de joueurs');
            $table->string('Nom du stade equipe');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identifiants');
    }
}
